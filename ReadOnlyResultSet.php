<?php 
if (!class_exists('ReadOnlyResultSet'))
{
	class ReadOnlyResultSet 
	{
		// This member variable will hold the native result set
		public $rs;
		public $currRow = 1;
		
		// Assign the native result set to an instance variable
		public function __construct(mysqli_result $rs)
		{
			$this->rs = $rs;
		}
		
		// Receives an instance of the DataObject we're working on
		public function getNext(DoRoBase $dataobject)
		{
			$row = $this->rs->fetch_array();
			if (empty($row))
			{
				return $dataobject;
			}
			
			// Fetch the DO's field names
			$properties = $dataobject->getProperties();
			
			// Loop through the properties to set them from the current row
			foreach ($properties as $prop_name)
			{
				$dataobject->$prop_name = $row[$prop_name];
			}
			
			return $dataobject;
		}
		
		public function getSame(DoRoBase $dataobject)
		{
			$this->currRow--;
			return $this->getNext($dataobject);
		}
		
		// Move the pointer back to the beginning of the result set
		public function reset()
		{
			$this->rs->data_seek(0);
		}
		
		// Return the number of rows in the result set
		public function rowCount()
		{
			return $this->rs->num_rows;
		}
	}
}