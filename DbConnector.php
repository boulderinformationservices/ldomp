<?php

/**
 * You will need to change the values in the __construct() method to reflect
 * your own database connection settings. Therefore, if you are including LDOMP
 * as a submodule, you should copy this file into your own codebase so that 
 * updates to LDOMP do not overwrite your settings.
 * 
 * Further, if you do update LDOMP, please be sure to recopy this file and 
 * replace the constructor with your own database settings.
 * 
 * It might be more convenient to separate the database settings from the 
 * connector object. LDOMP could then be a straight plugin/submodule. However,
 * by declaring the variables private, they are only defined and visible in one
 * place, and thus (slightly) more secure.
 */

class DbConnector 
{
	protected $connection;
	protected static $conn;
	
	private $mysql_charset = 'utf8';
	
	/* We recommend you set these values in the switch statement below, not here */
	private $hostname = "";
	private $database_name = "";
	private $username = "";
	private $password = "";
	
	public function __construct() 
	{
		if (array_key_exists('SERVER_NAME', $_SERVER))  
		{
			switch ($_SERVER['SERVER_NAME']) 
			{ 
				case "your_development_server":
				case "127.0.0.1":
				case "yoursite.local":
				case "www.yoursite.local":
					$this->hostname="";
					$this->database_name = "";
					$this->username = "";
					$this->password = "";
					break;
				case "your_acceptance_servers":
					$this->hostname="";
					$this->database_name="";
					$this->username="";
					$this->password="";
					break;
				case "your_production_servers":
				case "www.yoursite.com":
				case "yoursite.com":
				case "m.yoursite.com":
					$this->hostname="";
					$this->database_name="";
					$this->username="";
					$this->password="";
					break;
				default:
					// echo "DEBUG in DbConnector.php and server didn't match: " . $_SERVER['SERVER_NAME'];
			}
		} else {
			// echo "DEBUG in DbConnector.php and no server variable was set";
		}
	}
	
	public function getConnection()
	{
		if (isset(self::$conn))
		{
			return self::$conn;
		}
		
		if (!isset($this->connection))
		{
			$mysqli = new mysqli(
				$this->hostname,
				$this->username,
				$this->password,
				$this->database_name
			);
	
			if ($mysqli->connect_errno) {
			    echo 'PHPDO Error 5736: Unable to get database connection (' . $mysqli->connect_error .')';
			}
			
			$mysqli->set_charset($this->mysql_charset);
			$this->connection = $mysqli;
			self::$conn = $mysqli;
		}
		return $this->connection;
	}
}
