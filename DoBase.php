<?php 
/* based on http://onlamp.com/pub/a/php/2004/08/05/dataobjects.html?page=1 */
require_once('DoRoBase.php'); 
require_once('PHPDOException.php'); 

// this class is for read/write
class DoBase extends DoRoBase 
{
	// make sure to override these variables in classes extended from this class
	protected $tableName = "";			// name of the table in the database
	protected $primaryKeys = array();

	// now onto the methods
	public function __construct() 
	{
		parent::__construct();
	}
	
	public function del() 
	{
		$tableName = $this->tableName;
		$connection = $this->getConnection();
		$sWhere = $this->getWhereClause($this->primaryKeys);
		if (strpos(strtolower($sWhere), " where ") !== 0)
		{
			return false;
		}
		$sql = "DELETE FROM " . $tableName;
		$sql .= $sWhere;
		$dc = $connection->query($sql);
		if(!$dc)
		{
			error_log($sql);
			throw new PHPDOException('Could not delete record.', 7974);	
		}
		return $dc;
	}
	
	public function get($gracefulFailAttemptPk = -1)
	{
		if ($gracefulFailAttemptPk > -1 && count($this->primaryKeys) == 1)
		{
			$pk = $this->primaryKeys[0];
			$this->$pk = $gracefulFailAttemptPk;
		}
		$result = $this->getRawResultSet($this->primaryKeys);
		$row = $result->fetch_array();
		$properties = $this->getProperties();
		foreach ($properties as $name)
		{
			$this->$name = stripslashes($row[$name]);
		}
		return $this;
	}
	
	public function getIds()
	{
		$ids = array();
		if ($this->primaryKeys)
		{
			foreach ($this->primaryKeys as $pk) 
			{
				$ids[$pk] = $this->$pk;
			}
		}
		return $ids;
	}

	public function addIfDne() 	// DNE = Does Not Exist
	{
		// if an identical entry already exists in the DB
		// make $this object into the one from the DB (presumably we don't know
		// the ID ). Otherwise, insert $this into the DB
		$rs = $this->find();
		if ($rs->rowCount() > 0)
		{
			// make sure we have an exact match
			$arrProps = $this->getProperties();
			for ($i = 0; $i < $rs->rowCount(); $i++) 
			{
				$match = true;
				$thisClass = get_class($this);
				$testObj = $rs->getNext( new $thisClass() );
				foreach ($arrProps as $property)
				{
					// skip the primary key, which we probably don't know
					if (in_array($property, $this->primaryKeys)) 
					{
						continue;
					}
					$match = ($match && ($this->$property == $testObj->$property));
				}
				if ($match)
				{
					foreach ($this->primaryKeys as $pk) 
					{
						$this->$pk = $testObj->$pk;
					}
					return $testObj;
				}
			}
		}
		$this->insert();
		return $this;
	}

	protected function getInsertIgnoreSql()
	{
		return $this->getInsertSql("INSERT IGNORE INTO ");
	}

	protected function getInsertSql($insertPhrase = "INSERT INTO ")
	{
		$tableName = $this->tableName;
		$fields = array();
		$values = array();

		$sql = $insertPhrase . $tableName . " ";
		
		$properties = $this->getProperties();
		
		foreach ($properties as $name)
		{
			if ($this->$name != '') 
			{
				$fields[] = sprintf("`%s`", $name);
				$values[] = $this->quote_smart($this->$name);
			}
		}
		$sql .= "(".implode(',', $fields).")";
		$sql .= " VALUES ";
		$sql .= "(".implode(',', $values).")";
		return $sql;
	}

	public function insertIgnore()
	{
		$sql = $this->getInsertIgnoreSql();
		return $this->insert($sql);
	}

	public function insert($sql = null)
	{
		if ($sql === null)
		{
			$sql = $this->getInsertSql();
		}

		$connection = $this->getConnection();
		$success = $connection->query($sql);
		if (!$success)
		{
			error_log($connection->connect_error . "|" . $sql);
			throw new PHPDOException('Could not insert data.', 9287);
		}
		if ($connection->insert_id > 0) 
		{
			$pk = $this->primaryKeys[0];
			$this->$pk = $connection->insert_id;
		}
		return $success;
	}
	
	public function isNull()
	{
		$thisClass = get_class($this);
		$blank = new $thisClass();
		if ($this == $blank) { return true; }
		$blank->getProperties();
		
		return ($this == $blank);
	}

	public function insertOrUpdate() // uses update partial
	{
		$sql = $this->getInsertSql();
		$connection = $this->getConnection();
		$success = $connection->query($sql);
		if (!$success)
		{
			return $this->updatePartial();
		}
		if ($connection->insert_id > 0) 
		{
			$pk = $this->primaryKeys[0];
			$this->$pk = $connection->insert_id;
		}
		return $success;
	}

	// if a value is not set in the object, don't set it to null in the DB
	// e.g., updating an updated DATE only should not reset all other fields to ""
	public function updatePartial()
	{
		return $this->_update(true);
	}
	
	public function update() 
	{
		return $this->_update(false);
	}
	
	// to be overridden if certain fields (e.g., DateAdded) should not be updated
	protected function getUpdateProperties()
	{
		return $this->getProperties();
	}

	protected function _update($bPartialUpdate)
	{
		foreach ($this->primaryKeys as $pk) 
		{
			if (!isset($this->$pk) || $this->$pk == "") { return false; }
		}
		$properties = $this->getUpdateProperties();
		
		if (count(array_diff($properties, $this->primaryKeys)) < 1)
		{
			return true;
		}
		
		$tableName = $this->tableName;
		$connection = $this->getConnection();
		$fields = array();
		$fieldList = "";
		$values = array();
		$valueList = "";
		$separator="";
		$conn = $this->getConnection();
		
		$sql = "UPDATE " . $tableName . " SET ";
		foreach ($properties as $property) 
		{
			if (in_array($property, $this->primaryKeys)) { continue; }
			if ($bPartialUpdate && !isset($this->$property)) { continue; }
			$sql .= $separator . '`' . $property . "`=" . 
				$this->quote_smart($this->$property);
			$separator = ",";
		}
		$sql .= $this->getWhereClause($this->primaryKeys);
		$success = $connection->query($sql);
		if (!$success)
		{
			error_log($connection->error . "|" . $sql);
			throw new PHPDOException("Could not update data.", 9288);
		}
		return $success;
	}
	
	/**
	 * @param $fieldname
	 * @param $value
	 * @return object or NULL
	 */
	public static function getBy($fieldname, $value)
	{
		if (!property_exists(get_called_class(), $fieldname))
		{
			return NULL;
		}
		
		$search = new static();
		$search->$fieldname = $value;
		$rs = $search->find();
		if ($rs->rowCount() !== 1)
		{
			return NULL;
		}
		$obj = $rs->getNext(new static());
		return $obj;
	}
}
